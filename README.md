# SwipepayURLSchemeExample에 대하여 #

본앱은 스와이프페이앱을 다른 앱에서 URLScheme 방식으로 호출할 경우에 대한 이해를 돕기위해 작성된 샘플 앱입니다.
앱연동규약에 대한 정보를 얻고자 한다면 Download 링크를 따라가시거나 아래의 링크를 클릭하시면 됩니다.
본 샘플앱은 개발의 편의를 돕기위해 작성된 앱으로서 샘플앱의 로직을 그대로 따라서 개발하실 필요가 없습니다. 
앱연동규약에 나온 연동규약을 준수하여 제작사의 앱의 구성등에 따라 적절한 방법을 찾아 개발을 하시면됩니다.
또한 본 샘플앱의 결제앱 호출등 모든 로직에 대해서 스와이프페이는 품질보증을 하지 않습니다.

[스와이프페이 앱연동규약 다운로드](https://bitbucket.org/firstpayment/swipepayurlschemeexample/downloads)

**앱개발시 참고사항**

A라는 액티비티에서 저희 결제앱을 호출한 이후 결과값을 다시 A라는 액티비티로 받는 로직을 처리하고자 하는 경우에는
별다른 설정이 안되어있을경우 일반적으로 A라는 이름의 액티비티가 계속 새로 생성되게 됩니다. 
만일 A라는 액티비티가 추가 생성되지 않고 원래 호출한 액티비티에서 그대로 결과값을 받고자 하신다면 
샘플 소스(app-source) 상에서 아래와 같은 로직도 하나의 방법입니다.

1. AndroidManifest.xml의 액티비티설정의 android:launchMode="singleTask"
2. MainActivity의 onNewIntent 메소드

동일 액티비티에서 결과를 받는것이 아니라면 onResume,onCreate등 적절한 위치에 결과를 얻는
로직을 구성하시면 됩니다.

프래그먼트에서 콜백값을 가져올때는 MainActivity의 onNewIntent에 있는 아래 코드정보를 개발에 참고해주십시오.
```
#!java

BlankFragment fragment = null;
fragment=(BlankFragment)this.getSupportFragmentManager().findFragmentById(R.id.프래그먼트);
fragment.onNewIntent(intent);

```