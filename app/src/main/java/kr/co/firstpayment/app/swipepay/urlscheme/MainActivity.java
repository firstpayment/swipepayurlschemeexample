package kr.co.firstpayment.app.swipepay.urlscheme;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;

import layout.BlankFragment;

public class MainActivity extends AppCompatActivity implements BlankFragment.OnFragmentInteractionListener {

    private String scheme="fpswipepay://setle";
    private String schemeForReceipt="fpswipepay://rcipt";

    private String crtftCode="스와이프페이에서 테스트용 인증코드를 발급받아 이 변수의 값으로 사용하십시오.";

    private String mberCode; //상점회원코드는 테스트용 인증코드를 발급받은후 제공됩니다.
    private String splpc;
    private String vat;
    private String rciptNo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }

    @Override
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        Log.d("kmj", "onNewIntent");
        //URLScheme방식으로 호출된경우 결과정보를 얻어서 처리..
        if (Intent.ACTION_VIEW.equals(intent.getAction())) {
            Uri uri = intent.getData();

            try {

                String cardCashSe = uri.getQueryParameter("cardCashSe");
                String delngSe = uri.getQueryParameter("delngSe");
                String setleSuccesAt = uri.getQueryParameter("setleSuccesAt");
                String setleMssage= URLDecoder.decode(uri.getQueryParameter("setleMssage"), "UTF-8");

                String rciptNo = "";
                String confmNo = "";
                String confmDe = "";
                String confmTime = "";
                String cardNo = "";
                String instlmtMonth = "";
                String issuCmpnyCode;
                String issuCmpnyNm = "";
                String puchasCmpnyCode;
                String puchasCmpnyNm = "";
                String splpc = "";
                String vat ="";

                if(cardCashSe.equals("CARD")){
                    if(setleSuccesAt.equals("O")){
                        rciptNo = uri.getQueryParameter("rciptNo");
                        confmNo = uri.getQueryParameter("confmNo");
                        confmDe = uri.getQueryParameter("confmDe");
                        confmTime = uri.getQueryParameter("confmTime");
                        cardNo = uri.getQueryParameter("cardNo");
                        instlmtMonth = uri.getQueryParameter("instlmtMonth");
                        issuCmpnyCode = uri.getQueryParameter("issuCmpnyCode");
                        issuCmpnyNm = URLDecoder.decode(uri.getQueryParameter("issuCmpnyNm"), "UTF-8");
                        puchasCmpnyCode = uri.getQueryParameter("puchasCmpnyCode");
                        puchasCmpnyNm = URLDecoder.decode(uri.getQueryParameter("puchasCmpnyNm"), "UTF-8");
                        splpc = uri.getQueryParameter("splpc");
                        vat = uri.getQueryParameter("vat");
                    }
                }else if(cardCashSe.equals("CASH")){
                    if(setleSuccesAt.equals("O")){
                        rciptNo = uri.getQueryParameter("rciptNo");
                        confmNo = uri.getQueryParameter("confmNo");
                        confmDe = uri.getQueryParameter("confmDe");
                        confmTime = uri.getQueryParameter("confmTime");
                        cardNo = uri.getQueryParameter("cardNo");
                        instlmtMonth = uri.getQueryParameter("instlmtMonth");
                        splpc = uri.getQueryParameter("splpc");
                        vat = uri.getQueryParameter("vat");
                    }
                }

                ((TextView)this.findViewById(R.id.rciptNo)).setText(rciptNo);

                String aditInfo = URLDecoder.decode(uri.getQueryParameter("aditInfo"), "UTF-8");

                String 출력정보="공급가 : " + splpc +" 부가세 : " + vat +" 카드정보 : " + cardNo + "결제처리결과 : "+setleSuccesAt+" 결제메세지 : "+setleMssage+" 추가정보: "+aditInfo;

                Toast.makeText(this, 출력정보, Toast.LENGTH_LONG).show();
                Log.d("kmj", 출력정보);
                ////////////////////////////////////////////////////////////////
                //만일 프래그먼트에서 데이터를 받아야 한다면 아래의 방식을 참고하여 구현하면 된다.
                ////////////////////////////////////////////////////////////////
                BlankFragment fragment = (BlankFragment)this.getSupportFragmentManager().findFragmentById(R.id.프래그먼트);
                fragment.onNewIntent(intent);

                //영수증정보를 호출한다.
                try {

                    if(uri.getQueryParameter("delngSe").equals("0")) {
                        splpc = "-" + splpc;
                        vat = "-" + vat;
                    }

                    String url = schemeForReceipt+"?"+
                            "crtftCode="+crtftCode+
                            "&mberCode="+mberCode+
                            "&rciptNo="+rciptNo;

                    intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                    startActivity(intent);

                }catch(ActivityNotFoundException e) {
                    //해당하는 앱이없어 앱호출에 실패한 것이므로 마켓에서 앱을 다운로드 받도록 유도해야한다.
                    Intent marketLaunch = new Intent(Intent.ACTION_VIEW);
                    marketLaunch.setData(Uri.parse("market://details?id=kr.co.firstpayment.app.swipepay"));
                    this.startActivity(marketLaunch);
                }


            }catch(Exception e) {
                Toast.makeText(this, e.toString(), Toast.LENGTH_SHORT).show();
            }


        }
    }

    private void 데이터셋팅(){

        mberCode=((TextView)this.findViewById(R.id.mberCode)).getText().toString();
        splpc=((TextView)this.findViewById(R.id.splpc)).getText().toString();
        vat=((TextView)this.findViewById(R.id.vat)).getText().toString();
        rciptNo=((TextView)this.findViewById(R.id.rciptNo)).getText().toString();
    }

    public void 카드결제정보로앱호출(View view){

        데이터셋팅();

        try {

            String cardCashSe="CARD";
            String delngSe="1";
            String aditInfo= URLEncoder.encode("동해물과= 백두산이&마르=   고닳도록&하느님이=보우하사 우리나라만세","UTF-8");

            String url = scheme+"?"+
                         "crtftCode="+crtftCode+
                         "&mberCode="+mberCode+
                         "&cardCashSe="+cardCashSe+
                         "&delngSe="+delngSe+
                         "&splpc="+splpc+
                         "&vat="+vat+
                         "&aditInfo="+aditInfo;

            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            startActivity(intent);

        }catch(UnsupportedEncodingException e){

        }catch(ActivityNotFoundException e) {
            //해당하는 앱이없어 앱호출에 실패한 것이므로 마켓에서 앱을 다운로드 받도록 유도해야한다.
            Intent marketLaunch = new Intent(Intent.ACTION_VIEW);
            marketLaunch.setData(Uri.parse("market://details?id=kr.co.firstpayment.app.swipepay"));
            this.startActivity(marketLaunch);
        }
    }
    public void 카드취소정보로앱호출(View view){

        데이터셋팅();

        try {

            String cardCashSe="CARD";
            String delngSe="0";
            String aditInfo= URLEncoder.encode("동해물과= 백두산이&마르=   고닳도록&하느님이=보우하사 우리나라만세","UTF-8");

            String url = scheme+"?"+
                    "crtftCode="+crtftCode+
                    "&mberCode="+mberCode+
                    "&cardCashSe="+cardCashSe+
                    "&delngSe="+delngSe+
                    "&aditInfo="+aditInfo+
                    "&rciptNo="+rciptNo;

            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            startActivity(intent);

        }catch(UnsupportedEncodingException e){

        }catch(ActivityNotFoundException e) {
            //해당하는 앱이없어 앱호출에 실패한 것이므로 마켓에서 앱을 다운로드 받도록 유도해야한다.
            Intent marketLaunch = new Intent(Intent.ACTION_VIEW);
            marketLaunch.setData(Uri.parse("market://details?id=kr.co.firstpayment.app.swipepay"));
            this.startActivity(marketLaunch);
        }
    }
    public void 현금결제정보로앱호출(View view){

        데이터셋팅();

        try {

            String cardCashSe="CASH";
            String delngSe="1";
            String aditInfo= URLEncoder.encode("동해물과= 백두산이&마르=   고닳도록&하느님이=보우하사 우리나라만세","UTF-8");

            String url = scheme+"?"+
                    "crtftCode="+crtftCode+
                    "&mberCode="+mberCode+
                    "&cardCashSe="+cardCashSe+
                    "&delngSe="+delngSe+
                    "&splpc="+splpc+
                    "&vat="+vat+
                    "&aditInfo="+aditInfo;

            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            startActivity(intent);

        }catch(UnsupportedEncodingException e){

        }catch(ActivityNotFoundException e) {
            //해당하는 앱이없어 앱호출에 실패한 것이므로 마켓에서 앱을 다운로드 받도록 유도해야한다.
            Intent marketLaunch = new Intent(Intent.ACTION_VIEW);
            marketLaunch.setData(Uri.parse("market://details?id=kr.co.firstpayment.app.swipepay"));
            this.startActivity(marketLaunch);
        }
    }
    public void 현금취소정보로앱호출(View view){

        데이터셋팅();

        try {

            String cardCashSe="CASH";
            String delngSe="0";
            String aditInfo= URLEncoder.encode("동해물과= 백두산이&마르=   고닳도록&하느님이=보우하사 우리나라만세","UTF-8");

            String url = scheme+"?"+
                    "crtftCode="+crtftCode+
                    "&mberCode="+mberCode+
                    "&cardCashSe="+cardCashSe+
                    "&delngSe="+delngSe+
                    "&aditInfo="+aditInfo+
                    "&rciptNo="+rciptNo;

            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            startActivity(intent);

        }catch(UnsupportedEncodingException e){

        }catch(ActivityNotFoundException e) {
            //해당하는 앱이없어 앱호출에 실패한 것이므로 마켓에서 앱을 다운로드 받도록 유도해야한다.
            Intent marketLaunch = new Intent(Intent.ACTION_VIEW);
            marketLaunch.setData(Uri.parse("market://details?id=kr.co.firstpayment.app.swipepay"));
            this.startActivity(marketLaunch);
        }
    }

    public void 영수증호출(View view){

        데이터셋팅();

        try {

            String url = schemeForReceipt+"?"+
                    "crtftCode="+crtftCode+
                    "&mberCode="+mberCode+
                    "&rciptNo="+rciptNo;

            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            startActivity(intent);

        }catch(ActivityNotFoundException e) {
            //해당하는 앱이없어 앱호출에 실패한 것이므로 마켓에서 앱을 다운로드 받도록 유도해야한다.
            Intent marketLaunch = new Intent(Intent.ACTION_VIEW);
            marketLaunch.setData(Uri.parse("market://details?id=kr.co.firstpayment.app.swipepay"));
            this.startActivity(marketLaunch);
        }
    }

    @Override
    public void onFragmentInteraction(Uri uri) {
        Log.d("Main","onFragmentInteraction");
    }
}
